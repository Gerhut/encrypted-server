var crypto = require('crypto')
var fs = require('fs')
var connect = require('connect')
var send = require('send')
var serialNumber = require('serial-number')

var app = connect()
var serial = null

app.use(function (req, res) {
  var decipher = crypto.createDecipher('aes192', serial)
  fs.createReadStream('./' + req.url + '.crypted').on('error', function () {
    res.statusCode = 500
    res.end()
  }).pipe(decipher).pipe(res)
})

serialNumber(function (err, value) {
  if (err) {
    console.err(err.message)
    process.exit(-1)
  }

  serial = value

  app.listen(8765)
})
