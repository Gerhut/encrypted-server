var crypto = require('crypto')
var fs = require('fs')
var glob = require('glob')
var serialNumber = require('serial-number')

serialNumber(function (err, serial) {
  if (err) {
    console.error(err.message)
    process.exit(1)
  }

  glob('**', function (err, files) {
    if (err) {
      console.error(err.message)
      process.exit(1)
    }

    files.forEach(function (file) {
      var cipher = crypto.createCipher('aes192', serial)

      fs.createReadStream(file)
        .pipe(cipher)
        .pipe(fs.createWriteStream(file + '.crypted'))
    })
  })
})
